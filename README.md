# Metactl

A simple wrapper to manage different init systems

## Usage
```
metactl {COMMAND} <service-name>;
   COMMAND:
        help            Show this help text
        start           Start a service
        stop            Stop a service
        restart         Restart a service
        enable          Rnable a service
        disable         Disable a service
        status          Get the status of a service
        ls-files        List service files
        ls-running      List running services status
        ls-faild        List failed services
        ls-all          List available services
```
